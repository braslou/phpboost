	# INCLUDE MAINTAIN #
	<script src="{PATH_TO_ROOT}/templates/{THEME}/js/flaunt.js"></script>	
	<script src="{PATH_TO_ROOT}/templates/{THEME}/js/scroll-to.js"></script>


	
	<div id="top_page"></div>	
	<div id="contenu">	
	<header id="header">
		<div id="header-gsm">
			<a id="site-name-gsm" href="{PATH_TO_ROOT}/">{SITE_NAME}</a>
		</div>
		<div id="top-header">
			<div id="visit-counter">
				# IF C_VISIT_COUNTER #
				 ::&nbsp;<span>{L_VISIT} : {VISIT_COUNTER_TOTAL}&nbsp;-&nbsp;{L_TODAY} : {VISIT_COUNTER_DAY}</span>&nbsp;:: &nbsp; Bienvenue sur <a id="welcome-name" href="{PATH_TO_ROOT}/">{SITE_NAME}</a> !
				# ENDIF #
			</div>
			
			<div id="site-infos">
				<div id="site-logo"# IF C_HEADER_LOGO # style="background: url({HEADER_LOGO}) no-repeat;"# ENDIF #></div>
				<div id="site-name-container">
					<a id="site-name" href="{PATH_TO_ROOT}/">{SITE_NAME}</a>
					<span id="site-slogan">{SITE_SLOGAN}</span>
				</div>
			</div>
			<div id="top-header-content">
			# IF C_MENUS_HEADER_CONTENT #
				# START menus_header #
				{menus_header.MENU}
				# END menus_header #
			# ENDIF #
			</div>
			
			<section id="slider" class="demo">
				<!-- <a href="#" class="next"><i class="fa fa-chevron-right fa-2"></i></a>
				 <a href="#" class="prev"><i class="fa fa-chevron-left fa-2"></i></a> -->
				<div class="container">
					<!-- <div style="display: inline-block;"><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/1.jpg"/></a></div>
					<div><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/2.jpg"/></a></div>
					<div><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/3.jpg"/></a></div>
					<div><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/4.jpg"/></a></div>
					<div><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/5.jpg"/></a></div>
					<div><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/6.jpg"/></a></div> -->
					<!-- Photos d'été -->
					<div style="display: inline-block;"><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou2.jpg" alt="Vue générale Braslou"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/fontaine2.jpg" alt="Fontaine"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/faisan.jpg" alt="Faisan"></a></div>
                	<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/huppes.jpg" alt="Huppes"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/champ.jpg" alt="Champ"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/champ2.jpg" alt="Champ"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/champ3.jpg" alt="Champ"></a></div>
					<!-- Photos d'Hiver
					<div style="display: inline-block;"><a href="index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_01.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_02.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_03.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_04.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_05.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_06.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_07.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_08.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_09.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_10.jpg" alt="Braslou en hiver"></a></div>
					<div><a href="{PATH_TO_ROOT}/index.php"><img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/slide/braslou_hiver_11.jpg" alt="Braslou en hiver"></a></div> -->
				</div>
			</section>

			<div class="spacer"></div>
			<div id="prim-nav">
				<section id="navbook">
					<div id="menu-title">Menu principal</div>		
					<nav  class="MNnav">
							<ul id="MNnav" class="MNnav-list">
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}/"><i class="fa fa-home"></i> Accueil</a>
								</li>	
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}/news/"><i class="fa fa-newspaper-o"></i> Actualités</a>
								</li>
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}/articles/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Articles</a>
									<ul  class="MNnav-submenu">
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}articles/7-infos-utiles/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Infos Utiles</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}/articles/2-bulletins-municipaux/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Bulletins Municipaux</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}/articles/5-extrait-du-registre-des-deliberations-du-conseil-municipal/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Extraits du Registre</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}/articles/12-vie-de-la-commune/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Vie de la Commune</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}/articles/18-etat-civil/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> État Civil</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}/articles/4-resultats-electoraux/"><img src="/drapeau-france-icone-7111-32.png" border="0" width="15" height="15"> Résultats Électoraux</a>
										</li>
									</ul>
								</li>
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}?=portfolio"><i class="fa fa-bars"></i> Les sites</a>
									<ul  class="MNnav-submenu">
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}?=submenu-1">Site 1</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}?=submenu-2">Site 2</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}?=submenu-3">Site 3</a>
										</li>
										<li class="MNnav-submenu-item">
											<a href="{PATH_TO_ROOT}?=submenu-4">Site 3</a>
										</li>
									</ul>
								</li>
								<!-- <li class="MNnav-item">
									<a href="?=about"><i class="fa fa-rss"></i> Blog</a>
								</li>
								<li class="MNnav-item">
									<a href="?=about"><i class="fa fa-medkit"></i> F.A.Q</a>
								</li> -->						
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}/gallery/"><i class="fa fa-desktop"></i> Galerie de Photos</a>
								</li>
								<li class="MNnav-item">
									<a href="{PATH_TO_ROOT}/contact/"><i class="fa fa-envelope"></i> Contact</a>
								</li>
							</ul>
					</nav>
				</section>
			</div>
		</div>
		<div id="sub-header">
			# IF C_MENUS_SUB_HEADER_CONTENT #
				# START menus_sub_header #
				{menus_sub_header.MENU}
				# END menus_sub_header #
			# ENDIF #
		</div>
		<div class="spacer"></div>
	</header>
	
	<div id="menugo">
		<div id="gotop" style="display: block;">
			<a class="js-scrollTo" href="#top_page"><i class="fa fa-chevron-up"></i></a>
		</div>
		<div id="gobottom" style="display: block;">
			<a class="js-scrollTo" href="#bottom_page"><i class="fa fa-chevron-down"></i></a>
		</div>
	</div>		
	
	<div id="global">
		<div id="sous-global">

			# IF C_MENUS_LEFT_CONTENT #
			<aside id="menu-left">
				# START menus_left #
				{menus_left.MENU}
				# END menus_left #
			</aside>
			# ENDIF #

			<div id="main" class="# IF C_MENUS_LEFT_CONTENT #main-with-left# ENDIF ## IF C_MENUS_RIGHT_CONTENT # main-with-right# ENDIF #" role="main">
				# IF C_MENUS_TOPCENTRAL_CONTENT #
				<div id="top-content">
					# START menus_top_central #
					{menus_top_central.MENU}
					# END menus_top_central #
				</div>
				<div class="spacer"></div>
				# ENDIF #

				<div id="main-content" itemprop="mainContentOfPage">
					# INCLUDE ACTIONS_MENU #
					<nav id="breadcrumb" itemprop="breadcrumb">
						<ol>
							<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
								<a href="{START_PAGE}" title="{L_INDEX}" itemprop="url">
									<span itemprop="title">{L_INDEX}</span>
								</a>
							</li>						
							# START link_bread_crumb #
							<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" # IF link_bread_crumb.C_CURRENT # class="current" # ENDIF #>
								<a href="{link_bread_crumb.URL}" title="{link_bread_crumb.TITLE}" itemprop="url">
									<span itemprop="title">{link_bread_crumb.TITLE}</span>
								</a>
							</li>
							# END link_bread_crumb #
						</ol>
					</nav>
					# INCLUDE KERNEL_MESSAGE #
					{CONTENT}
				</div>

				# IF C_MENUS_BOTTOM_CENTRAL_CONTENT #
				<div id="bottom-content">
					# START menus_bottom_central #
					{menus_bottom_central.MENU}
					# END menus_bottom_central #
				</div>
				# ENDIF #
			</div>

			# IF C_MENUS_RIGHT_CONTENT #
			<aside id="menu-right">
				# START menus_right #
				{menus_right.MENU}
				# END menus_right #
			</aside>
			# ENDIF #

			# IF C_MENUS_TOP_FOOTER_CONTENT #
			<div id="top-footer">
				# START menus_top_footer #
				{menus_top_footer.MENU}
				# END menus_top_footer #
				<div class="spacer"></div>
			</div>
			# ENDIF #

			<div class="spacer"></div>
		</div>
	</div>
	
	</div><!-- fermeture du "Id:contenu"-->
	
	<footer id="footer">

		<div id="copy"><!-- Menu de liens en bas de page -->
			<ul class="copmenu" style=" display: inline;">
				<li><a href="{PATH_TO_ROOT}/sitemap" title="Plan du site">Plan du site</a></li>
				<li><a href="{PATH_TO_ROOT}/pages/mentions-legales" title="Mentions Légales"></a><a href="{PATH_TO_ROOT}/pages/mentions-legales" title="Mentions Légales">Mentions Légales</a></li>
				<li><a href="http://www.service-public.fr/" target="_blank" title="Service-Public.fr">Service-Public.fr</a></li>
				<!-- <li><a href="{PATH_TO_ROOT}/user/login/" title="Connexion">Connexion</a></li> -->
			</ul>
			<div class="spacer"></div>
		</div>

		# IF C_MENUS_FOOTER_CONTENT #
		<div class="footer-content">
			# START menus_footer #
			{menus_footer.MENU}
			# END menus_footer #
		</div>
		# ENDIF #

		<div class="footer-infos">
			<span class="cms">
				{L_POWERED_BY} <a href="http://www.phpboost.com" title="{L_PHPBOOST_LINK}">PHPBoost</a> {L_PHPBOOST_RIGHT}
			</span>
			# IF C_DISPLAY_BENCH #
			<span class="requete">
			<span class="footer-infos-separator"> | </span>{L_ACHIEVED} {BENCH}{L_UNIT_SECOND} - {REQ} {L_REQ} - {MEMORY_USED}
			</span>
			# ENDIF #
			# IF C_DISPLAY_AUTHOR_THEME #
			<span class="author">
			<span class="footer-infos-separator"> | </span>{L_THEME} {L_THEME_NAME} {L_BY}
				<a href="{U_THEME_AUTHOR_LINK}">{L_THEME_AUTHOR}</a>
			</span>
			# ENDIF #
		</div>

	</footer>
	

	<div id="bottom_page"></div>
